﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemies;

    private Transform tf;

    private int ranSpawn;
    private int amountOfEnemies;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (amountOfEnemies < 1)
        {
            StartCoroutine(SpawnTime());
        }
    }

    void EnemySpawn()
    {
        ranSpawn = Random.Range(0, 10);

        if (ranSpawn > 6)
        {
            Instantiate(enemies[0], tf.position, tf.rotation, tf.transform);
            amountOfEnemies++;
        }
        else
        {
            Instantiate(enemies[1], tf.position, tf.rotation);
            amountOfEnemies++;
        }
    }

    IEnumerator SpawnTime()
    {
        EnemySpawn();
        yield return new WaitForSeconds(30);
        EnemySpawn();
    }
}
