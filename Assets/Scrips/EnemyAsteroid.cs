﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAsteroid : MonoBehaviour
{
    public CharacterController target;

    public float moveSpeed;
    public float enemyHealth;

    private Transform tf;
    private Respawn respawn;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        target = FindObjectOfType<CharacterController>();
        respawn = FindObjectOfType<Respawn>();
    }

    // Update is called once per frame
    void Update()
    {
        tf.position = Vector3.MoveTowards(tf.position, target.transform.position, moveSpeed * Time.deltaTime);

        if (enemyHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void TakeHealth(float rh)
    {
        enemyHealth += rh;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<CharacterController>())
        {
            if (respawn.lives > 0)
            {
                respawn.PlayerRespawn();
                respawn.TakeLives(-1);
            }
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
    }
}
