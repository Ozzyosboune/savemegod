﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public GameObject playerPrefab;

    public int lives;

    public GameObject gameOver;

    private CharacterController player;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (lives < 0)
        {
            gameOver.gameObject.SetActive(true);
        }
    }

    public void PlayerRespawn()
    {
        Instantiate(playerPrefab, transform.position, transform.rotation);
    }

    public void TakeLives(int tl)
    {
        lives += tl;
    }
}
