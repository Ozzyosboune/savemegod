﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public float speed;
    public float rotateSpeed;

    private Transform tf;

    private float rotateStep;

    public bool isDead = false;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead == false)
        {
            rotateStep = rotateSpeed * Time.deltaTime;

            if (Input.GetAxisRaw("Vertical") > 0)
            {
                tf.position += tf.right * speed * Time.deltaTime;
            }
            if (Input.GetAxisRaw("Vertical") < 0)
            {
                tf.position -= tf.right * speed * Time.deltaTime;
            }
            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                tf.Rotate(0, 0, -rotateStep);
            }
            if (Input.GetAxisRaw("Horizontal") < 0)
            {
                tf.Rotate(0, 0, rotateStep);
            }
        }
    }
}
