﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed;
    public float damage;
    public float bulletDeath;

    private Transform tf;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        StartCoroutine(BulletDeath());
    }

    // Update is called once per frame
    void Update()
    {
        tf.position -= tf.up * bulletSpeed * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<EnemyShip>())
        {
            other.gameObject.GetComponent<EnemyShip>().TakeHealth(-damage);
            Destroy(this.gameObject);
        }
        if (other.gameObject.GetComponent<EnemyAsteroid>())
        {
            other.gameObject.GetComponent<EnemyAsteroid>().TakeHealth(-damage);
            Destroy(this.gameObject);
        }
    }

    IEnumerator BulletDeath()
    {
        yield return new WaitForSeconds(bulletDeath);
        Destroy(this.gameObject);
    }
}
